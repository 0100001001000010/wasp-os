#!/usr/bin/env python

"""Generate 16 bit colors given 24 colors. Used because the channels are 5/6/5 bits which do not line up cleanly with hex characters, which represent 4 bits.
24 bit colors: bin: rrrr rrrr gggg gggg bbbb bbbb
               hex:    r    r    g    g    b    b

16 bit colors: bin: rrrr rggg gggb bbbb
                         |-------|--------misalignment
I don't feel like anyone should have to deal with that manually, so this tool will convert automatically.
"""

while True:
    userColor = input('Enter a color: ').strip('# ')[0:6]

    # Split the color into channels
    red, green, blue = userColor[0:2], userColor[2:4], userColor[4:6]

    # Convert the colors into binary
    def toBin(hex): return bin(int(hex, 16))[2:].zfill(8)
    red, green, blue = toBin(red), toBin(green), toBin(blue)

    # Truncate the binary colors to 5, 6, 5
    red, green, blue = red[:5], green[:6], blue[:5]

    # Concatenate and convert back to hex
    convertedColor = hex(int(f'{red}{green}{blue}', 2))

    print(convertedColor)
