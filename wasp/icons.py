# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

# 2-bit RLE, generated from res/battery.png, 104 bytes
battery = (
    b'\x02'
    b'\x18 '
    b'\x04\x01\x02\xca\x0c\xce\n\xce\t\xd0\x04\xc8\x08\xd0\x08\xd0'
    b'\x08\xd0\x08\xcc\x04\x08\x04\xc8\x10\xc8\t\xc3\x04\xc8\x08\xc4'
    b'\x04\xc8\x07\xc5\x04\xc8\x06\xc5\x05\xc8\x05\xc5\x06\xc8\x04\xc5'
    b'\x01@\xfcA\x05\xc8\x03\xcb\x02\xc8\x02\xcc\x02\xc8\x02\xcc'
    b'\x02\xc8\x02\xcb\x03\xc8\x05\x01\x01\xc5\x04\xc8\x06\xc5\x05\xc8'
    b'\x05\xc5\x06\xc8\x04\xc5\x07\xc8\x04\xc4\x08\xc8\x04\xc3\t\xc8'
    b'\x10\xc8P\xff%'
)

# 2-bit RLE, generated from res/bomb.png, 100 bytes
bomb = (
    b'\x02'
    b'  '
    b'\x15\xc2\x06\xc22\xc3\x03\xc2\x02\xc2\x13\xc1\x03\xc1\x1a\xc1'
    b'\x05\xc5\x15\xc1\x1c\xc7\x04\xc2\x02\xc2\x0f\xc7\x19\xc7\x02\xc2'
    b'\x06\xc2\r\xc7\x17\xcb\x13\xcf\x10\xc6\x02\xc9\x0e\xd3\r\xd3'
    b'\x0c\xc5\x02\xce\x0b\xc4\x02\xcf\x0b\xd5\n\xc4\x01\xd2\t\xc3'
    b'\x02\xd2\t\xc3\x01\xd3\t\xc3\x02\xd2\t\xc4\x01\xd2\n\xd5'
    b'\x0b\xd5\x0b\xd5\x0c\xd3\r\xd3\x0e\xd1\x10\xcf\x13\xcb\x18\xc5'
    b'\x0e'
)

# 2-bit RLE, generated from res/app_icon.png, 224 bytes
app = (
    b'\x02'
    b'`@'
    b'\x1e@\x81d<d<d;f?X\xec2\xf0/'
    b'\xf2-\xf4,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3+\xc4.\xc3*'
    b'\xc5.\xc3*\xc5.\xc3*\xc5.\xc3*\xc5.\xc3*'
    b'\xc5.\xc3*\xc5.\xc3*\xc5.\xc3+\xc4.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xf4-\xf2/'
    b'\xf02\xec?Xf;d<d<d\x1e'
)

# 2-bit RLE, 96x64, generated from res/clock_icon.png, 419 bytes
clock = (
    b'\x02'
    b'`@'
    b'\x1e@\x81d<d<d;f?X\xec2\xf0/'
    b'\xf2-\xf4,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
    b'\xc3.\xc3,\xc3.\xc3,\xc3\x02\x01\x02\x80\xeb\x84\x04'
    b'\xc4\x0e\x82\x06\xc3\x04\xc3,\xc3\x02\x01\x01\x81\x03\x82\x03'
    b'\xc1\x02\xc1\rA\x01\x81\x05\xc1\x07\xc3,\xc3\x02\x01\x06'
    b'\x81\x02\xc1\x04\xc1\x0e\x81\x04\xc1\x08\xc3,\xc3\x02\x01\x06'
    b'\x81\x02\xc1\x04\xc1\x05\x81\x08\x81\x04\xc1\x01\xc3\x04\xc3,'
    b'\xc3\x02\x01\x05\x82\x02\xc1\x04\xc1\x05\x81\x08\x81\x04\xc2\x02'
    b'\xc2\x03\xc3+\xc4\x02\x01\x04\x82\x03\xc1\x04\xc1\x0e\x81\x04'
    b'\xc2\x03\xc1\x03\xc3*\xc5\x02\x01\x03\x82\x04\xc1\x04\xc1\x0e'
    b'\x81\x04\xc1\x04\xc1\x03\xc3*\xc5\x02\x01\x02\x82\x05\xc1\x04'
    b'\xc1\x0e\x81\x04\xc1\x04\xc1\x03\xc3*\xc5\x02\x01\x01\x82\x07'
    b'\xc1\x02\xc2\x05\x81\x08\x81\x04\xc2\x02\xc2\x03\xc3*\xc5\x02'
    b'\x01\x01\x86\x03\xc4\x06\x81\x06\x85\x03\xc4\x04\xc3*\xc5\x02'
    b'\x01+\xc3*\xc5\x02\x01+\xc3*\xc5\x02\x01+\xc3+'
    b'\xc4\x02\x01+\xc3,\xc3\x02\x01+\xc3,\xc3\x02\x01+'
    b'\xc3,\xc3\x02\x01+\xc3,\xc3\x02\x01\x07\xc2\x05\xc2\x08'
    b'\xc2\x01\xc2\x0e\xc3,\xc3\x02\x01\n\xc1\xc1\x02\xc1\x01\xc1'
    b'\x02\xc2\x0b\xc1\xc1\xc1\x08\xc3,\xc3\x02\x01\x0e\xc1\xc1\xc1'
    b'\x03\xc1\x03\xc1\x03\xc1\x01\xc1\x0c\xc3,\xc3.\xc3,\xc3'
    b'.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3'
    b'.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3'
    b'.\xc3,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,\xf4'
    b'-\xf2/\xf02\xec?Xf;d<d<d\x1e'
)

# 2-bit RLE, generated from res/up_arrow.png, 19 bytes
up_arrow = (
    b'\x02'
    b'\x10\t'
    b'\x07\xc2\r\xc4\x0b\xc6\t\xc8\x07\xca\x05\xcc\x03\xce\x01\xe0'
)

# 2-bit RLE, generated from res/down_arrow.png, 19 bytes
down_arrow = (
    b'\x02'
    b'\x10\t'
    b'\xe0\x01\xce\x03\xcc\x05\xca\x07\xc8\t\xc6\x0b\xc4\r\xc2\x07'
)

# 2-bit RLE, generated from res/knob.png, 72 bytes
knob = (
    b'\x02'
    b'(('
    b'\x10\xc8\x1c\xd0\x16\xd4\x13\xd6\x10\xda\r\xdc\x0b\xde\t\xe0'
    b'\x08\xe0\x07\xe2\x05\xe4\x04\xe4\x03\xe6\x02\xe6\x02\xe6\x02\xe6'
    b'\x01\xff\xff\x02\x01\xe6\x02\xe6\x02\xe6\x02\xe6\x03\xe4\x04\xe4'
    b'\x05\xe2\x07\xe0\x08\xe0\t\xde\x0b\xdc\r\xda\x10\xd6\x13\xd4'
    b'\x16\xd0\x1c\xc8\x10'
)

# 2-bit RLE, generated from res/notification.png, 104 bytes
notification = (
    b'\x02'
    b'\x1e '
    b'\x0e\xc2\x1b\xc4\x1a\xc4\x18\xc8\x14\xcc\x11\xce\x0f\xd0\x0e\xc5'
    b'\x06\xc5\r\xc4\n\xc4\x0c\xc4\n\xc4\x0b\xc4\x0c\xc4\n\xc4'
    b'\x0c\xc4\n\xc4\x0c\xc4\n\xc3\r\xc4\t\xc4\x0e\xc3\t\xc4'
    b'\x0e\xc4\x08\xc4\x0e\xc4\x08\xc4\x0e\xc4\x08\xc4\x0e\xc4\x08\xc3'
    b'\x0f\xc4\x07\xc4\x10\xc4\x06\xc4\x10\xc4\x06\xc4\x10\xc4\x05\xc5'
    b'\x10\xc4\x05\xc4\x12\xc4\x03\xc5\x12\xc5\x02\xdc\x01\xfc\x01\xdc'
    b'\x0e\xc4\x1b\xc2\x0e'
)

# 2-bit RLE, generated from res/blestatus.png, 104 bytes
blestatus = (
    b'\x02'
    b'\x16 '
    b'\x07\xc1\x15\xc2\x14\xc3\x13\xc4\x12\xc5\x11\xc6\x10\xc7\x0f\xc3'
    b'\x01\xc4\x08\xc2\x04\xc3\x02\xc4\x06\xc4\x03\xc3\x03\xc4\x06\xc4'
    b'\x02\xc3\x02\xc4\x08\xc4\x01\xc3\x01\xc4\n\xcb\x0c\xc9\x0e\xc7'
    b'\x10\xc5\x11\xc5\x10\xc7\x0e\xc9\x0c\xcb\n\xc4\x01\xc3\x01\xc4'
    b'\x08\xc4\x02\xc3\x02\xc4\x06\xc4\x03\xc3\x03\xc4\x06\xc2\x04\xc3'
    b'\x02\xc4\r\xc3\x01\xc4\x0e\xc7\x0f\xc6\x10\xc5\x11\xc4\x12\xc3'
    b'\x13\xc2\x14\xc1\x0e'
)

# 2-bit RLE, generated from res/checkbox.png, 108 bytes
checkbox = (
    b'\x02'
    b'  '
    b'\x02\xdc\x03\xde\x01\xe4X\xc7Z\xc6Z\xc6Z\xc6T\x82'
    b'D\xc6S\x84C\xc6R\x86B\xc6Q\x86C\xc6P\x86'
    b'D\xc6O\x86E\xc6N\x86F\xc6M\x86G\xc6L\x86'
    b'H\xc6D\x82E\x86I\xc6C\x84C\x86J\xc6B\x86'
    b'A\x86K\xc6C\x8bL\xc6D\x89M\xc6E\x87N\xc6'
    b'F\x85O\xc6G\x83P\xc6H\x81Q\xc6Z\xc6Z\xc6'
    b'Z\xc7X\xe4\x01\xde\x03\xdc\x02'
)
