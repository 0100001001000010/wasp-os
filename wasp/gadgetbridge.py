# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson
"""Gadgetbridge/Bangle.js protocol

Currently implemented messages are:

 * t:"notify", id:int, src,title,subject,body,sender,tel:string - new
   notification
 * t:"notify-", id:int - delete notification
 * t:"alarm", d:[{h,m},...] - set alarms
 * t:"find", n:bool - findDevice
 * t:"vibrate", n:int - vibrate
 * t:"weather", temp,hum,txt,wind,loc - weather report
 * t:"musicstate", state:"play/pause",position,shuffle,repeat - music
   play/pause/etc
 * t:"musicinfo", artist,album,track,dur,c(track count),n(track num) -
   currently playing music track
 * t:"call", cmd:"accept/incoming/outgoing/reject/start/end", name: "name", number: "+491234" - call
"""

import wasp

# JSON compatibility
# null = None
# true = True
# false = False


def GB(cmd):
    try:
        if cmd['t'] == 'find':
            wasp.watch.vibrator.pin(not cmd['n'])
        elif cmd['t'] == 'notify':
            wasp.system.notify(cmd['id'], cmd)
            wasp.watch.vibrator.pulse(ms=wasp.system.notify_duration)
        elif cmd['t'] == 'notify-':
            wasp.system.unnotify(cmd['id'])
        elif cmd['t'] == 'musicstate':
            wasp.system.toggle_music(cmd)
        elif cmd['t'] == 'musicinfo':
            wasp.system.set_music_info(cmd)
        elif cmd['t'] == 'weather':
            wasp.system.set_weather_info(cmd)
        else:
            pass
    except Exception as e:
        pass
