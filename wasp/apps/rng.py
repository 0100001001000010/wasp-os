# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

import wasp
from random import randint


class RandomApp():
    """A random number generator app, useful for deciding things randomly in social situations, assuming everyone trusts you not to have biased your watch."""
    NAME = 'Random'
    # 2-bit RLE, 96x64, generated from res/random_icon.png, 209 bytes
    ICON = (
        b'\x02'
        b'`@'
        b'?N\xc8?\x16\xce?\x11\xd3?\r\xc4\x08\xc9?\x0b'
        b'\xc3\x0c\xc9?\x08\xc3\x0e\xc9?\x06\xc3\x10\xc8?\x06\xc3'
        b'\x10\xc9?\x04\xc4\x11\xc8?\x04\xc4\x12\xc8?\x02\xc6\x11'
        b'\xc8?\x02\xc6\x11\xc8?\x02\xc7\x10\xc9?\x01\xc8\x0f\xc9'
        b'?\x01\xc8\x0f\xc9?\x01\xc8\x0f\xc9?\x02\xc7\x0f\xc9?'
        b'\x02\xc7\x0f\xc8?\x04\xc5\x10\xc8?\x19\xc8?\x18\xc8?'
        b'\x19\xc8?\x19\xc8?\x19\xc7?\x19\xc8?\x19\xc7?\x1a'
        b'\xc6?\x1a\xc7?\x1a\xc6?\x1a\xc6?\x1b\xc5?\x1c\xc4'
        b'?\x1c\xc5?\x1c\xc4?\x1c\xc4?\x1d\xc3?\x1e\xc3?'
        b'\x1d\xc3?\x1e\xc3?\x1e\xc2?\x1e\xc3?\x1e\xc3?\x1e'
        b'\xc2?\x1f\xc2?\x1f\xc2?\x1f\xc2?\x1f\xc1?\xff\xff'
        b'b\xc1?\x1e\xc6?\x1a\xc8?\x18\xc9?\x18\xc9?\x18'
        b'\xca?\x17\xc9?\x18\xc9?\x19\xc7?\x1b\xc5,'
    )

    def __init__(self):
        """Initialize the application."""
        self._types = ['Coin', 'Dice', 'Card']
        self._currentType = 0
        self._typeString = 'Coin'
        self.scrollIndicator = wasp.widgets.ScrollIndicator()

    def foreground(self):
        """Activate the application."""
        self._draw()
        wasp.system.request_event(wasp.EventMask.TOUCH |
                                  wasp.EventMask.SWIPE_UPDOWN)

    def background(self):
        """De-activate the application."""
        pass

    def swipe(self, event):
        """Notify the application of a touchscreen swipe event."""
        if event[0] == wasp.EventType.UP:
            self._currentType += 1
        else:
            self._currentType -= 1
        if self._currentType % len(self._types) == 0:
            self._currentType = 0
        self._typeString = self._types[self._currentType]

        self._draw()

    def touch(self, event):
        """Notify the application of a touchscreen touch event."""
        self._update()

    def _draw(self):
        """Draw the display from scratch."""
        draw = wasp.watch.drawable
        draw.fill()
        draw.string(self._typeString, 0, 6, width=240)
        self.scrollIndicator.draw()
        self._update()

    def _update(self):
        wasp.watch.vibrator.pulse(duty=50, ms=100)
        draw = wasp.watch.drawable
        if self._typeString == 'Coin':
            draw.string('Heads' if randint(0, 1) else 'Tails', 0, 108, 240)
        elif self._typeString == 'Dice':
            draw.string(str(randint(1, 6)), 0, 108, 240)
        elif self._typeString == 'Card':
            rng = randint(0, 51)
            value = ['Ace', '2', '3', '4', '5', '6', '7', '8',
                     '9', '10', 'Jack', 'Queen', 'King'][rng // 4]
            suit = ['Spade', 'Diamond', 'Club', 'Heart'][rng // 13]
            draw.string('{} of {}s'.format(value, suit), 0, 108, 240)
        # else:
        #     pass

    def sleep(self):
        return True
