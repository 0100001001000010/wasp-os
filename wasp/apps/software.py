# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson
"""Software
~~~~~~~~~~~

A tool to enable/disable applications.

.. figure:: res/SoftwareApp.png
    :width: 179

Most applications are disabled by default at boot in order to conserve
RAM (which is in short supply and very useful to anyone wanting to
write an application). This tools allows us to boot and conserve RAM
whilst still allowing users to activate so many awesome applications!
"""

import wasp


class SoftwareApp():
    """Enable and disable applications."""
    NAME = 'Software'
    # 2-bit RLE, generated from res/software_icon.png, 460 bytes
    ICON = (
        b'\x02'
        b'`@'
        b'\x1e@\x81d<d<d;f?X\xec2\xf0/'
        b'\xf2-\xf4,\xc3.\xc3,\xc3.\xc3,\xc3.\xc3,'
        b'\xc3.\xc3,\xc3.\xc3,\xc3\x0c\x80\xd2\x83\x10\xc0C'
        b'\xc3\x0c@\xd7C,C\n\x87\x0c\xc7\nC,C\t'
        b'\x83\x02\x84\n\xc4\x02\xc3\tC,C\x08\x82\x07\x82\x08'
        b'\xc2\x07\xc2\x08C,C\x07\x82\t\x82\x06\xc2\t\xc2\x07'
        b'C,C\x06\x82\x0b\x82\x04\xc2\x0b\xc2\x06C,C\x06'
        b'\x82\x0b\x82\x04\xc2\x0b\xc2\x06C,C\x05\x82\x0c\x82\x04'
        b'\xc2\x0c\xc2\x05C,C\x05\x82\x0c\x82\x04\xc2\x0c\xc2\x05'
        b'C,C\x05\x83\x0b\x82\x04\xc2\x0b\xc3\x05C,C\x06'
        b'\x82\x0b\x82\x04\xc2\x0b\xc2\x06C,C\x06\x82\x0b\x82\x04'
        b'\xc2\x0b\xc1\x07C,C\x07\x82\n\x82\x04\xc2\n\xc2\x07'
        b'C+D\x08\x82\t\x82\x04\xc2\t\xc2\x08C*E\t'
        b'\x8c\x04\xcc\tC*E\n\x8b\x04\xcb\nC*E.'
        b'C*E.C*E.C*E.C*E\n'
        b'\x80\xbb\x8b\x04\xc0X\xcb\nC+D\t\x8c\x04\xcc\t'
        b'C,C\x08\x82\t\x82\x04\xc2\t\xc2\x08C,C\x07'
        b'\x82\n\x82\x04\xc2\n\xc2\x07C,C\x06\x82\x0b\x82\x04'
        b'\xc2\x0b\xc1\x07C,C\x06\x82\x0b\x82\x04\xc2\x0b\xc2\x06'
        b'C,C\x05\x83\x0b\x82\x04\xc2\x0b\xc3\x05C,C\x05'
        b'\x82\x0c\x82\x04\xc2\x0c\xc2\x05C,C\x05\x82\x0c\x82\x04'
        b'\xc2\x0c\xc2\x05C,C\x06\x82\x0b\x82\x04\xc2\x0b\xc2\x06'
        b'C,C\x06\x82\x0b\x82\x04\xc2\x0b\xc2\x06C,C\x07'
        b'\x82\t\x82\x06\xc2\t\xc2\x07C,C\x08\x82\x07\x82\x08'
        b'\xc2\x07\xc2\x08C,C\t\x83\x02\x84\n\xc4\x02\xc3\t'
        b'C,C\n\x86\x0e\xc6\nC,C\x0c\x83\x10\xc3\x0c'
        b'C,C.C,C.C,C.C,C.'
        b'C,C.C,t-r/p2l?X@'
        b'\x81f;d<d<d\x1e'
    )

    def foreground(self):
        """Activate the application."""

        def factory(label):
            nonlocal y

            cb = wasp.widgets.Checkbox(0, y, label)
            y += 40
            if y > 160:
                y = 0
            return cb

        y = 0
        db = []
        # db.append(('alarm', factory('Alarm')))                    # I enabled this by default in wasp.py, you may want to remove it there before enabling here
        # db.append(('calc', factory('Calculator')))                # I enabled this by default in wasp.py, you may want to remove it there before enabling here
        # db.append(('faces', factory('Faces')))                    # I don't use this. Also nable in manifest if enabling here.
        # db.append(('gameoflife', factory('Game Of Life')))        # I don't use this. Also nable in manifest if enabling here.
        # db.append(('haiku', factory('Contact Info')))             # I don't use this. Also nable in manifest if enabling here.
        # db.append(('freemem', factory('Free Memory')))            # I enabled this by default in wasp.py, you may want to remove it there before enabling here
        # db.append(('musicplayer', factory('Music Player')))
        db.append(('play2048', factory('Play 2048')))
        # db.append(('pomodoro', factory('Pomodoro')))
        # db.append(('rng', factory('Random')))
        db.append(('snake', factory('Snake Game')))
        # db.append(('sports', factory('Sports')))
        # db.append(('flashlight', factory('Light')))               # I enabled this by default in wasp.py, you may want to remove it there before enabling here
        # db.append(('timer', factory('Timer')))                    # I enabled this by default in wasp.py, you may want to remove it there before enabling here
        # db.append(('weather', factory('Weather')))

        # Handle user-loaded applications
        try:
            for app in wasp.os.listdir('apps'):
                name = None
                if app.endswith('.py'):
                    name = app[:-3]
                if app.endswith('.mpy'):
                    name = app[:-4]
                if name:
                    db.append((name, factory(name)))
        except OSError:
            # apps does not exist...
            pass

        # Get the initial state for the checkboxes
        for _, checkbox in db:
            label = checkbox.label.replace(' ', '')
            for app in wasp.system.launcher_ring:
                if type(app).__name__.startswith(label):
                    checkbox.state = True
                    break

        self.si = wasp.widgets.ScrollIndicator()
        self.page = 0
        self.db = db

        self._draw()
        wasp.system.request_event(wasp.EventMask.TOUCH |
                                  wasp.EventMask.SWIPE_UPDOWN)

    def background(self):
        self.si = None
        del self.si
        self.page = None
        del self.page
        self.db = None
        del self.db

    def get_page(self):
        i = self.page * 5
        return self.db[i:i+5]

    def swipe(self, event):
        """Notify the application of a touchscreen swipe event."""
        page = self.page
        pages = (len(self.db)-1) // 5
        if event[0] == wasp.EventType.DOWN:
            page = page - 1 if page > 0 else pages
        if event[0] == wasp.EventType.UP:
            page = page + 1 if page < pages else 0
        self.page = page

        mute = wasp.watch.display.mute
        mute(True)
        self._draw()
        mute(False)

    def touch(self, event):
        """Notify the application of a touchscreen touch event."""
        for module, checkbox in self.get_page():
            if checkbox.touch(event):
                label = checkbox.label.replace(' ', '')
                if checkbox.state:
                    wasp.gc.collect()
                    wasp.system.register('apps.{}.{}App'.format(module, label))
                else:
                    for app in wasp.system.launcher_ring:
                        if type(app).__name__.startswith(label):
                            wasp.system.launcher_ring.remove(app)
                            del app
                            break
                    wasp.gc.collect()
                break

    def _draw(self):
        """Draw the display from scratch."""
        wasp.watch.drawable.fill()
        self.si.draw()
        for _, checkbox in self.get_page():
            checkbox.draw()
