# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Wolfgang Ginolas
"""Timer Application
~~~~~~~~~~~~~~~~~~~~

An application to set a vibration in a specified amount of time. Like a kitchen timer.

    .. figure:: res/TimerApp.png
        :width: 179

        Screenshot of the Timer Application

"""

import wasp
import time
import widgets
import math
from micropython import const

# 2-bit RLE, generated from res/timer_icon.png, 345 bytes
icon = (
    b'\x02'
    b'`@'
    b'?\xff\r@\xb4I?\x14Q?\rW?\x08[?'
    b'\x04_?\x00c<Q\x80\xd0\x83Q:L\x87\x01\x87'
    b'L8K\x89\x01\x89K6J\x8b\x01\x8bJ4I\x8d'
    b'\x01\x8dI2I\x9dI0I\x83\x01\x97\x01\x83I.'
    b'H\x86\x01\x95\x01\x86H-H\xa3H,H\x92\x01\x92'
    b'H+G\x92\x03\x92G*G\x92\x05\x92G)G\x92'
    b"\x05\x92G(G\x82\x01\x8f\x07\x8f\x01\x82G'G\x83"
    b"\x01\x8e\x07\x8e\x01\x83G'F\x93\x07\x93F&G\x93"
    b'\x07\x93G%F\x94\x07\x94F%F\x94\x07\x94F%'
    b'F\x94\x07\x94F$G\x94\x07\x94G#G\x94\x07\x94'
    b'G#G\x94\x07\x94G#F\x95\x07\x95F#F\x81'
    b'\x04\x90\x07\x90\x04\x81F#F\x95\x07\x95F#G\x94'
    b'\x07\x94G#G\x94\x07\x94G#G\x94\x07\x94G$'
    b'F\x94\x07\x94F%F\x94\x07\x94F%F\x94\x07\x94'
    b"F%G\x93\x07\x93G%G\x93\x07\x93F'G\x83"
    b"\x01\x8e\x07\x8e\x01\x83G'G\x82\x01\x8f\x07\x8f\x01\x82"
    b'G(G\x92\x05\x92G)G\x93\x03\x93G*G\xa7'
    b'G+H\xa5H,G\xa5G-H\x86\x01\x9cH.'
    b'H\x84\x01\x96\x01\x85H0I\x9a\x01\x82I1J\x8d'
    b'\x01\x8dJ2K\x8b\x01\x8bK4K\x8a\x01\x89L5'
    b'N\x87\x01\x87N5S\x85S5k5k5k5'
    b'k5k5k\x1b'
)

_STOPPED = const(0)
_RUNNING = const(1)
_RINGING = const(2)

_BUTTON_Y = const(200)

fields = ('789C'
          '4560'
          '123E')


class TimerApp():
    """Allows the user to set a vibration alarm.
    """
    NAME = 'Timer'
    ICON = icon

    def __init__(self):
        """Initialize the application."""
        self.current_alarm = None
        self.setTime = 0  # Numerical time value entered by user
        self.state = _STOPPED

    def foreground(self):
        """Activate the application."""
        self._draw()
        wasp.system.request_event(wasp.EventMask.TOUCH)
        wasp.system.request_tick(1000)

    def background(self):
        """De-activate the application."""
        if self.state == _RINGING:
            self.state = _STOPPED

    def tick(self, ticks):
        """Notify the application that its periodic tick is due."""
        if self.state == _RINGING:
            wasp.watch.vibrator.pulse(duty=50, ms=500)
            wasp.system.keep_awake()
        if self.state == _RUNNING:
            wasp.system.keep_awake()
        self._update()

    def touch(self, event):
        """Notify the application of a touchscreen touch event."""
        if self.state == _RINGING:
            mute = wasp.watch.display.mute
            mute(True)
            self._stop()
            mute(False)
        elif self.state == _RUNNING and event[2] >= 200:
            self._stop()
        else:  # _STOPPED
            # Code mostly copied from calculator, with modifications
            if (event[2] < 48):
                if (event[1] > 200):  # undo button pressed
                    self.setTime = self.setTime // 10
            else:
                x = event[1] // 63
                y = (event[2] // 63) - 1

                # Error handling for touching at the border
                if x > 3:
                    x = 3
                if y > 2:
                    y = 2
                button_pressed = fields[x + 4*y]
                if (button_pressed == "C"):
                    self.setTime = 0
                elif (button_pressed == "E"):
                    self._start()
                elif (button_pressed == " "):
                    pass
                else:
                    self.setTime *= 10
                    self.setTime += int(button_pressed)
            self._update()

    def _start(self):
        self.state = _RUNNING
        now = wasp.watch.rtc.time()
        hours = self.setTime // 10000
        minutes = (self.setTime % 10000) // 100
        seconds = self.setTime % 100
        self.current_alarm = now + (hours * 3600) + (minutes * 60) + seconds
        wasp.system.set_alarm(self.current_alarm, self._alert)
        self._draw()

    def _stop(self):
        self.state = _STOPPED
        wasp.system.cancel_alarm(self.current_alarm, self._alert)
        self._draw()

    def _draw(self):
        """Draw the display from scratch."""
        draw = wasp.watch.drawable
        draw.reset()
        draw.fill()

        if self.state == _RINGING:
            draw.set_font(wasp.fonts.sans24)
            draw.string(self.NAME, 0, 150, width=240)
            draw.blit(icon, 73, 50)
            sbar = wasp.system.bar
            sbar.clock = True
            sbar.draw()
        elif self.state == _RUNNING:
            self._draw_stop(104, _BUTTON_Y)
            draw.string(':', 110, 120-14, width=20)
            sbar = wasp.system.bar
            sbar.clock = True
            sbar.draw()
            self._update()
        else:  # _STOPPED
            # Code pretty much copied from calculator
            hi = wasp.system.theme('bright')
            lo = wasp.system.theme('mid')
            mid = draw.lighten(lo, 2)
            bg = draw.darken(wasp.system.theme(
                'ui'), wasp.system.theme('contrast'))
            bg2 = draw.darken(bg, 2)

            # Draw the background
            draw.fill(0, 0, 0, 239, 47)
            draw.fill(0, 236, 239, 3)
            draw.fill(bg, 0, 48, 239, 235-48)

            # Make grid:
            draw.set_color(lo)
            for i in range(3):
                # horizontal lines
                draw.line(x0=0, y0=i*63+47, x1=239, y1=i*63+47)
                # vertical lines
                draw.line(x0=i*60+60, y0=47, x1=i*60+60, y1=236)
            draw.line(x0=0, y0=47, x1=0, y1=236)
            draw.line(x0=239, y0=47, x1=239, y1=236)
            draw.line(x0=0, y0=236, x1=239, y1=236)

            # Draw button labels
            draw.set_color(hi, bg)
            for x in range(4):
                if x == 3:
                    draw.set_color(mid, bg)
                for y in range(3):
                    label = fields[x + 4*y]
                    if (x == 0):
                        draw.string(label, x*60+24, y*60+72)
                    else:
                        draw.string(label, x*60+24, y*60+72)
            draw.set_color(hi)
            draw.string("<", 215, 10)
            self._update()

    def _update(self):
        draw = wasp.watch.drawable
        if self.state == _STOPPED:
            wasp.watch.drawable.string(
                str(self.setTime), 0, 14, width=200, right=True)
        else:
            wasp.system.bar.update()
            if self.state == _RUNNING:
                now = wasp.watch.rtc.time()
                s = self.current_alarm - now
                if s < 0:
                    s = 0
                m = str(math.floor(s // 60))
                s = str(math.floor(s) % 60)
                if len(m) < 2:
                    m = '0' + m
                if len(s) < 2:
                    s = '0' + s
                draw.set_font(wasp.fonts.sans28)
                draw.string(m, 0, 120-14, width=110, right=True)
                draw.string(s, 130, 120-14, width=60)

    # def _draw_play(self, x, y):
    #     draw = wasp.watch.drawable
    #     for i in range(0, 20):
    #         draw.fill(0xffff, x+i, y+i, 1, 40 - 2*i)

    def _draw_stop(self, x, y):
        wasp.watch.drawable.fill(0xffff, x, y, 40, 40)

    def _alert(self):
        self.state = _RINGING
        wasp.system.wake()
        wasp.system.switch(self)
