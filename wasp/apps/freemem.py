# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

"""Self Tests
~~~~~~~~~~~~~

A collection of tests used to develop features or provide useful metrics such
as performance indicators or memory usage.

.. figure:: res/SelfTestApp.png
    :width: 179
"""

import wasp


class FreeMemoryApp():
    """Self test application."""
    NAME = 'Memory'
    ICON = wasp.icons.app

    def foreground(self):
        """Activate the application."""
        self._draw()
        wasp.system.request_event(wasp.EventMask.TOUCH)

    def touch(self):
        self._draw()

    def _draw(self):
        """Redraw the display from scratch."""
        draw = wasp.watch.drawable
        draw.fill()

        draw.string("Boot: {}".format(wasp.watch.free), 12, 3*24)
        draw.string("Init: {}".format(wasp.free), 12, 4*24)
        draw.string("Now: {}".format(wasp.gc.mem_free()), 12, 5*24)
        wasp.gc.collect()
        draw.string("GC: {}".format(wasp.gc.mem_free()), 12, 6*24)
