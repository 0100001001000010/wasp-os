# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

"""Digital clock
~~~~~~~~~~~~~~~~

Shows a time (as HH:MM) together with a battery meter and the date.

.. figure:: res/ClockApp.png
    :width: 179
"""

import wasp

import fonts.clock as digits
from time import mktime

DIGITS = (
    digits.clock_0, digits.clock_1, digits.clock_2, digits.clock_3,
    digits.clock_4, digits.clock_5, digits.clock_6, digits.clock_7,
    digits.clock_8, digits.clock_9
)

MONTH = 'JanFebMarAprMayJunJulAugSepOctNovDec'


class ClockApp():
    """Simple digital clock application."""
    NAME = 'Clock'
    ICON = wasp.icons.clock
    _timeHash = -1

    def __init__(self):  # Do some stuff that I want to run on boot that I couldn't find a better place for
        # Contact info notification
        wasp.system.notify(0, {'title': 'title', 'body': 'body'})
        # Day/night modes
        _now = wasp.watch.rtc.get_localtime()

        def _dayMode():
            _now = wasp.watch.rtc.get_localtime()
            wasp.system.wake()
            wasp.system.brightness = 3
            wasp.system.set_alarm(
                mktime((_now[0], _now[1], _now[2], 21, 0, 0, 0, 0, 0)), _nightMode)

        def _nightMode():
            _now = wasp.watch.rtc.get_localtime()
            wasp.system.wake()
            wasp.system.brightness = 1
            wasp.system.set_alarm(
                mktime((_now[0], _now[1], _now[2] + 1, 7, 0, 0, 0, 0, 0)), _dayMode)
        if _now[3] >= 7 and _now[3] <= 21:
            wasp.system.brightness = 3
            wasp.system.set_alarm(
                mktime((_now[0], _now[1], _now[2], 21, 0, 0, 0, 0, 0)), _nightMode)
        else:
            wasp.system.brightness = 1
            wasp.system.set_alarm(
                mktime((_now[0], _now[1], _now[2] + 1, 7, 0, 0, 0, 0, 0)), _dayMode)

    def foreground(self):
        """Activate the application.

        Configure the status bar, redraw the display and request a periodic
        tick callback every second.
        """
        wasp.system.bar.clock = False
        self._timeHash = 0  # Guarantee a redraw
        self._draw(True)
        wasp.system.request_tick(1000)
        wasp.system.request_event(wasp.EventMask.TOUCH)

    def sleep(self):
        """Prepare to enter the low power mode.

        : returns: True, which tells the system manager not to automatically
                  switch to the default application before sleeping.
        """
        return True

    def wake(self):
        """Return from low power mode.

        Time will have changes whilst we have been asleep so we must
        udpate the display(but there is no need for a full redraw because
        the display RAM is preserved during a sleep.
        """
        self._draw()

    def tick(self, ticks):
        """Periodic callback to update the display."""
        self._draw()

    def preview(self):
        """Provide a preview for the watch face selection."""
        wasp.system.bar.clock = False
        self._draw(True)

    def touch(self, event):
        wasp.system.sleep()

    def _draw(self, redraw=False):
        """Draw or lazily update the display.

        The updates are as lazy by default and avoid spending time redrawing
        if the time on display has not changed. However if redraw is set to
        True then a full redraw is be performed.
        """
        draw = wasp.watch.drawable
        hi = wasp.system.theme('bright')
        # I don't really like mixing colors on my watch face. I just set all 3 colors to the same value so this is easy to undo
        lo = hi  # wasp.system.theme('mid')
        mid = hi  # draw.lighten(lo, 1)
        now = wasp.watch.rtc.get_localtime()

        if redraw:

            # Clear the display and draw that static parts of the watch face
            draw.fill()
            draw.blit(digits.clock_colon, 2*48, 60, fg=mid)
            # draw.blit(digits.clock_colon, 5*30, 101, fg=mid)
            # if self.updateMotd:
            #     try:
            #         motd = open('motd.txt')
            #         self._motd = motd.read().strip()
            #         motd.close()
            #         self.updateMotd = False
            #     except:
            #         pass
            # draw.string(self._motd, 0, 40, width=240)

            # Redraw the status bar
            wasp.system.bar.draw()
        else:
            wasp.system.bar.update()

        # Draw the changeable parts of the watch face
        # Update the seconds unconditionally
        draw.blit(DIGITS[now[5]//10], 72, 120,
                  fg=lo)   # Seconds tens digit
        draw.blit(DIGITS[now[5] % 10], 120, 120,
                  fg=hi)    # Seconds ones digit
        if (now[4] + 60*now[3] + 60*24 *
            now[2] + 60*24*31*now[1] + 60*24*31*12 *
                now[0]) != self._timeHash:  # If the time other than seconds has changed, update the rest
            draw.blit(DIGITS[now[3]//10], 0*48, 60,
                      fg=lo)   # Hours tens digit
            draw.blit(DIGITS[now[3] % 10], 1*48, 60,
                      fg=hi)    # Hours ones digit
            draw.blit(DIGITS[now[4]//10], 3*48, 60,
                      fg=lo)   # Minutes tens digit
            draw.blit(DIGITS[now[4] % 10], 4*48, 60,
                      fg=hi)    # Minutes ones digit
            draw.set_color(hi)
            # Date
            wkdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            draw.string('{}, {}/{}/{}'.format(wkdays[now[6]], now[1], now[2], now[0]),
                        0, 201, width=240)
            # # Weather
            # try:
            #     self._temp = int(wasp.system.weatherinfo.get('temp'))
            #     weatherString = '{}, {}'.format(
            #         str(int((self._temp - 273)*1.8+32)),
            #         wasp.system.weatherinfo.get('txt'))
            #     bounds = draw.wrap(weatherString, 240)
            #     draw.string(
            #         weatherString[bounds[0]:bounds[1]], 0, 40+24*0, width=240)
            #     if len(bounds) >= 3:
            #         draw.string(
            #             weatherString[bounds[1]:bounds[2]], 0, 40+24*1, width=240)
            #     else:  # Make sure we blank the second line if it isn't needed
            #         draw.string("", 0, 40+24*1, width=240)
            # except:
            #     draw.string('Weather error', 0, 40, width=240)
            self._timeHash = now[4] + 60*now[3] + 60*24 * \
                now[2] + 60*24*31*now[1] + 60*24*31*12 * \
                now[0]  # Simple value guaranteed to increase by at least 1 every time a minute passes.
